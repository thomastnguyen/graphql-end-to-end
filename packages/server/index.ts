import * as express from "express";
import * as bodyParser from "body-parser";
import { graphqlExpress, graphiqlExpress } from "apollo-server-express";
import { makeExecutableSchema } from "graphql-tools";

// Some fake data
const books = [
  {
    title: "Harry Potter and the Sorcerer's stone",
    authorId: 1
  },
  {
    title: "Jurassic Park",
    authorId: 2
  }
];

const authors = [
  {
    id: 1,
    name: "J.K. Rowling"
  },
  {
    id: 2,
    name: "Michael Crichton"
  }
];

// The GraphQL schema in string form
const typeDefs = `
  type Query { books: [Book], authors: [Author] }
  type Book { title: String, author: Author }
  type Author {id: ID, name: String, books: [Book] }
`;

// The resolvers
const resolvers = {
  Query: { books: () => books, authors: () => authors },
  Book: {
    author: book => {
      return authors.find(author => author.id === book.authorId);
    }
  },
  Author: {
    books: author => {
      return books.filter(book => book.authorId === author.id);
    }
  }
};

// Put together a schema
const schema = makeExecutableSchema({
  typeDefs,
  resolvers
});

// Initialize the app
const app = express();

// The GraphQL endpoint
app.use("/graphql", bodyParser.json(), graphqlExpress({ schema }));

// GraphiQL, a visual editor for queries
app.use("/graphiql", graphiqlExpress({ endpointURL: "/graphql" }));

// Start the server
app.listen(3000, () => {
  console.log("Go to http://localhost:3000/graphiql to run queries!");
});
